'use strict'

// recebendo o express exportado de bin
const app = require('./bin/app');
const variables = require('./bin/config/variables');


//Roda a Api na porta configurada em bin/config/variables
app.listen(variables.api.port, () => {
	console.log(`Servidor iniciado na porta ${variables.api.port}!`);
});