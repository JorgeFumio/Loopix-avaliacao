'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/celulares-controller');

let ctrl = new controller();

//Rotas Restful Chamando seus respectivos controllers
//Rota para get all
router.get('/', ctrl.get);

//Rota para get by id
router.get('/:id', ctrl.getById);

//Rota para create
router.post('/', ctrl.post);

//Rota para update
router.put('/:id', ctrl.put);

//Rota para update
router.patch('/:id', ctrl.patch);

//Rota para delete
router.delete('/:id', ctrl.delete);


module.exports = router;