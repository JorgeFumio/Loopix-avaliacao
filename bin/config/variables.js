const variables = {
	api: {
		port: process.env.port || 3000
	},
	database: {
		connection: process.env.connection || "mongodb://admin123:admin123@ds249942.mlab.com:49942/loopix-avaliacao"
	}
}


module.exports = variables;