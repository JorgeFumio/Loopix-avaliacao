//importando o framework express e o bodyparser para tratar corpo de requisições em JSON
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//arquivo variables com informações de configurações gerais como string de conexão com banco e porta default do server
const variables = require('./config/variables');

//importando os Routers
const celularesRouter = require('../routes/celulares-routes');


// inicializando o express
const app = express();


//Configurando o express para receber e enviar requisições em JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


//Configurando conexão com o banco
mongoose.connect(variables.database.connection);


//DEFININDO AS ROTAS com os sufixos dos routers declarados acima (novas rotas devem ser definidas aqui)
app.use('/api/celulares', celularesRouter);
app.get('/', function(req, res) {
  res.sendFile(__dirname+'\\home.html');
});


//exporta o express (Aplicação)
module.exports = app;