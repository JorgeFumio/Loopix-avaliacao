'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

//Modelagem da coleção que será criada no MongoDB
const celularesModel = new schema({
	// id: 			{ type: String, required: true, trim: true, index: true }, não necessita ID o banco cria sozinho
	modelo: 		{ type: String, required: true },
	marca: 			{ type: String, required: true },
	ano: 			{ type: Number, required: true },
	descricao: 		{ type: String, required: true },
	vendido: 		{ type: Boolean, required: true },
	created: 		{ type: Date },
	updated: 		{ type: Date }
}, {versionKey: false});

// gatilho antes de criação do objeto inicializa campo created com hora e data atual do servidor do banco de dados
celularesModel.pre('save', function(next) {
	let agora = new Date();
	if (!this.created) {
		this.created = agora;
	}
	next();
});


module.exports = mongoose.model('celulares', celularesModel);