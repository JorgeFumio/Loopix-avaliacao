'use strict'

const celularesRepository = require('../repositorios/celulares-repositorio');
const celRepInstance = new celularesRepository();

function celularesController() {}

//retorna todos registros
celularesController.prototype.get = async function(req, res) {
	try{
		var resultado = await celRepInstance.read();
		res.status(200).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
};

//retorna 1 registro por id
celularesController.prototype.getById = async function(req, res) {
	try {
		var resultado = await celRepInstance.readOne(req.params.id);
		res.status(200).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
};

//cria novo registro e retorna o registro criado
celularesController.prototype.post = async function(req, res) {
	try {
		var resultado = await celRepInstance.create(req.body);
		res.status(201).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
	
};

//atualiza todos os campos do registro para o id informado e retorna o registro alterado
celularesController.prototype.put = async function(req, res) {
	try {
		req.body.updated = new Date();
		var resultado = await celRepInstance.updateAll(req.params.id, req.body);
		res.status(202).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
};

//atualiza o status vendido para o status informado e seta o campo 'updated' para o momento da ação, e retorna o registro alterado
celularesController.prototype.patch = async function(req, res) {
	try {
		var inserir = {
			vendido: req.body.vendido,
			updated: new Date()
		};
		var resultado = await celRepInstance.updateOne(req.params.id, inserir);
		res.status(202).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
};

//deleta um registro para o id informado
celularesController.prototype.delete = async function(req, res) {
	try {
		var resultado = await celRepInstance.deletar(req.params.id);
		res.status(204).send(resultado);
	} catch(err) {
		res.status(400).send(`Oops, Sua requisição não foi realizada pelos seguintes motivos: \n${err.message}`);
	}
};


module.exports = celularesController;