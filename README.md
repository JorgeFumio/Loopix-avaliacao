# Loopix - API celulares

API Restful desenvolvida em [NodeJs](https://nodejs.org/en/).
Com Framework [Express](http://expressjs.com) ^4.16.3.

## Instalação

1. Download [NodeJS](https://nodejs.org/en/) 8.11.4 (LTS na data do desenvolvimento).
2. Execute `git clone git@gitlab.com:JorgeFumio/Loopix-avaliacao.git`.
3. cd `path/to/project`.
3. rode o comando `npm install` para restaurar as dependencias via NPM.

## Iniciando a aplicação	

```bash
node server
```
Acesse `http://localhost:3000`.

## Configuração de banco de dados

O banco está online num servidor gratuito para MongoDB (https://mlab.com/) (Usuario: JorgeFumio / Senha: 7iwlqjj7). A string de conexão do framework fica em configurada em `bin/config/variables.js` na chave `database: {connection: xxxxx }`.

## Endpoints

### Celulares
- Listar todos: `* GET * /api/celulares/`
- Listar um: `* GET * /api/celulares/:id`
- Cadastrar: `* POST * /api/celulares/  => {
		:modelo,
		:marca,
		:ano,
		:descricao,
		:vendido
	}`
- Editar o campo `vendido`: `* PATCH * /api/celulares/:id => {
		:vendido(true/false)
	}`
- Editar todos os Campos: `* PUT * /api/celulares/:id => {
		:modelo,
		:marca,
		:ano,
		:descricao,
		:vendido
	}`
- Deletar um registro: `* DELETE * /api/celulares/:id`


REQUEST HEADERS
- Content-Type: x-www-form-urlencoded

RESPONSE HEADERS
- Content-Type: application/json


