require('../models/celulares-model');

const mongoose = require('mongoose');
const celModel = mongoose.model('celulares');


// Classe com metodos(CRUD) para operações de banco de dados do model celulares
class celularesRepositorio {

	constructor() {

	}

	async read() {
		return await celModel.find();
	}

	async readOne(id) {
		return await celModel.findById(id);
	}
	
	async create(data) {
		let novo = new celModel(data);
		return await novo.save();
	}

	async updateAll(id, data) {
		await celModel.findByIdAndUpdate(id, { $set: data });
		return await celModel.findById(id);
	}

	async updateOne(id, data) {
		await celModel.findByIdAndUpdate(id, { $set: data });
		return await celModel.findById(id);
	}

	async deletar(id) {
		return await celModel.findByIdAndRemove(id);
	}

}


module.exports = celularesRepositorio;